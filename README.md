# BitBucket assignement
 application based on bitbucket-api that let the users login, view there profile and reposatries.   


# Features

* mvvm-architecture 
* repository-pattern
* koin
* viewmodel-pattern
* rxjava2
* retrofit
* multi module app 
* clean Architecture 
* livedata-viewmodel
* lifecycle
* lifecycleobserver
