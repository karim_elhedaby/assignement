package com.rowaad.bitbucketapitask

import android.app.Application
import com.photoweather.app.di.dataModule
import com.photoweather.app.utils.ReleaseTree
import com.rowaad.bitbucketapitask.di.mainModule


import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.BuildConfig
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(listOf(dataModule , mainModule))
        }

        if (BuildConfig.DEBUG) {

            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }
    }
}