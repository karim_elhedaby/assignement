package com.rowaad.bitbucketapitask.ui.reposatries

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.photoweather.app.data.ui_models.repo.Repository
import com.photoweather.app.replaceFragmentToActivity
import com.photoweather.app.utils.secretB
import com.photoweather.app.utils.showB
import com.rowaad.bitbucketapitask.R
import com.rowaad.bitbucketapitask.ui.repo_details.RepositoryDetailsFragment
import com.rowaad.bitbucketapitask.ui.reposatries.adapter.RepoAdapter
import com.rowaad.bitbucketapitask.utils.MyToast
import kotlinx.android.synthetic.main.fragment_repos.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RepositoriesFragment : Fragment(), RepoAdapter.RepoListener {

    var layoutManager: GridLayoutManager? = null
    var adapter: RepoAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_repos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val viewModel: RepositoriesViewModel by viewModel()

        viewModel.getRepos()

        setUpRecyclerView()
        handleErrorMsg(viewModel)
        handleFetchRepos(viewModel)
    }


    private fun setUpRecyclerView() {
        layoutManager = GridLayoutManager(context, 1)
        reposRecyclerView.layoutManager = layoutManager
        adapter = RepoAdapter(mutableListOf(), this)
        reposRecyclerView.adapter = adapter
    }

    private fun handleFetchRepos(viewModel: RepositoriesViewModel) {
        viewModel.getReposResults().observe(
            viewLifecycleOwner, Observer { repos ->
                if (repos.reposatries.isNullOrEmpty())
                    emptyLayout.showB()
                else {
                    emptyLayout.secretB()
                    adapter?.swapData(receivedData = repos.reposatries)
                }
            })
    }


    private fun handleErrorMsg(viewModel: RepositoriesViewModel) {
        viewModel.handleErrorMsg().observe(
            viewLifecycleOwner, Observer { errorMsg ->
                MyToast.showError(requireActivity(), errorMsg)
            })
    }


    override fun onClickRepo(repository: Repository) {
        activity?.supportFragmentManager?.let {
            replaceFragmentToActivity(
                it,
                RepositoryDetailsFragment.newInstance(repository)
            )
        }
    }

    companion object {
        fun newInstance() = RepositoriesFragment()
    }

}



