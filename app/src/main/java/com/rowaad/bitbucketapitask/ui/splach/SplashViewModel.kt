package com.rowaad.bitbucketapitask.ui.splach

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.photoweather.app.data.DataManager
import org.koin.core.KoinComponent

class SplashViewModel(
    private val dataManager: DataManager
) : ViewModel(), KoinComponent {

    private val isUserLogged: MutableLiveData<Boolean> =
        MutableLiveData<Boolean>()


    fun getUserStatus(): LiveData<Boolean> {
        isUserLogged.value = dataManager.isUserLogged()
        return isUserLogged
    }
}