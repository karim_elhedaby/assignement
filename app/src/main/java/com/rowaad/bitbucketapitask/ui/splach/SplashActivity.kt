package com.rowaad.bitbucketapitask.ui.splach;

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.rowaad.bitbucketapitask.R

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.rowaad.bitbucketapitask.ui.main.MainActivity
import com.rowaad.bitbucketapitask.ui.login.LoginActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val viewModel: SplashViewModel by viewModel()

        Handler(Looper.getMainLooper()).postDelayed(
            {
                handleNextScreen(viewModel)
            },
            2000
        )
    }

    private fun handleNextScreen(viewModel: SplashViewModel) {
        viewModel.getUserStatus().observe(
            this, Observer { isUserLogged ->
                if (isUserLogged) {
                    startActivity(MainActivity.getStartIntent(this@SplashActivity))
                    this@SplashActivity.finish()
                } else {
                    startActivity(LoginActivity.getStartIntent(this@SplashActivity))
                    this@SplashActivity.finish()
                }

            })
    }
}