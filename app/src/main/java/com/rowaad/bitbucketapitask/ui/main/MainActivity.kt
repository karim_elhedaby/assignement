package com.rowaad.bitbucketapitask.ui.main

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.photoweather.app.replaceFragmentToActivity
import com.rowaad.bitbucketapitask.R
import com.rowaad.bitbucketapitask.ui.profile.ProfileFragment
import com.rowaad.bitbucketapitask.ui.reposatries.RepositoriesFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initBottomNav()
    }


    private fun initBottomNav() {

        mainBottomNavigation.selectedItemId = R.id.repositoriesNav

        replaceFragmentToActivity(supportFragmentManager, RepositoriesFragment.newInstance())

        mainBottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {

                R.id.repositoriesNav -> {
                    replaceFragmentToActivity(
                        supportFragmentManager,
                        RepositoriesFragment.newInstance()
                    )

                }

                R.id.profileNav -> {
                    replaceFragmentToActivity(
                        supportFragmentManager,
                        ProfileFragment.newInstance()
                    )
                }

            }

            true
        }

    }

    companion object {
        fun getStartIntent(context: Context): Intent =
            Intent(context, MainActivity::class.java)
    }
}