package com.rowaad.bitbucketapitask.ui.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.photoweather.app.Common.genAuthKey
import com.photoweather.app.data.DataManager
import com.photoweather.app.data.ui_models.UserModel
import com.photoweather.app.inteceptor.ConnectivityStatus
import com.rowaad.bitbucketapitask.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject


class LoginViewModel(
    val dataManager: DataManager
) : ViewModel(), KoinComponent {

    var disposable: Disposable? = null

    private val loginResults: MutableLiveData<UserModel> =
        MutableLiveData<UserModel>()

    private val msgError: MutableLiveData<String> =
        MutableLiveData<String>()

    val context: Context by inject()

    fun login(userName: String, password: String) {
        if (ConnectivityStatus.isConnected(context)) {
            when {
                userName.isEmpty() -> msgError.value = context.getString(R.string.invalid_username)
                password.isEmpty() -> msgError.value = context.getString(R.string.invalid_password)
                else -> {

                    //generate base 64 base token
                    val basicToken = "Basic "+ genAuthKey(userName, password)

                    disposable =
                        dataManager.login(basicToken ?: "")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeBy(
                                onNext = {
                                    loginResults.value = it
                                    dataManager.user = it
                                    dataManager.userToken = basicToken

                                    Log.e("ADfsd", it.toString())

                                },
                                onError = {
                                    msgError.value = it.message.toString()
                                })
                }
            }
        } else {
            msgError.postValue("Please connect internet")
        }
    }

    fun getLoginResults(): LiveData<UserModel> {
        return loginResults
    }

    fun handleErrorMsg(): LiveData<String> {
        return msgError
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}
