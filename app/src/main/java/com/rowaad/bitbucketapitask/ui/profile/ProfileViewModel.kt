package com.rowaad.bitbucketapitask.ui.profile

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.photoweather.app.data.DataManager
import com.photoweather.app.data.ui_models.UserModel
import com.photoweather.app.data.ui_models.repo.ReposResponse
import com.photoweather.app.inteceptor.ConnectivityStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject


class ProfileViewModel(
    val dataManager: DataManager
) : ViewModel(), KoinComponent {

    var disposable: Disposable? = null

    private val profileResult: MutableLiveData<UserModel> =
        MutableLiveData<UserModel>()

    private val msgError: MutableLiveData<String> =
        MutableLiveData<String>()


    val context: Context by inject()

    fun logout() {
        dataManager.logout()
    }

    fun getProfile() {
        if (ConnectivityStatus.isConnected(context)) {
            when {
                else -> {
                    disposable =
                        dataManager.getMyProfile(
                            basicToken = dataManager.userToken ?: ""
                        )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeBy(
                                onNext = {
                                    profileResult.value = it

                                },
                                onError = {
                                    msgError.value = it.message.toString()
                                })
                }
            }
        } else {
            msgError.postValue("Please connect internet")
        }
    }

    fun getProfileResults(): LiveData<UserModel> {
        return profileResult
    }

    fun handleErrorMsg(): LiveData<String> {
        return msgError
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

}
