package com.rowaad.bitbucketapitask.ui.repo_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.photoweather.app.data.ui_models.repo.Repository
import com.photoweather.app.data.utils.Constants
import com.rowaad.bitbucketapitask.R
import kotlinx.android.synthetic.main.fragment_repo_details.*

class RepositoryDetailsFragment : Fragment() {


    var repository: Repository? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_repo_details, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        repository = arguments?.getSerializable(Constants.REPO_BUNDLE) as Repository

        fillData(repository)

    }

    private fun fillData(item: Repository?) {
        repoNameTV.text = "Slug : " + item?.slug
        repoFullNameTV.text = "FullName : " + item?.full_name
        repoDescriptionTV.text = "Description : " + item?.description

        repoTypeTV.text = "Type : " + item?.type
    }


    companion object {
        fun newInstance(repository: Repository): RepositoryDetailsFragment {
            val fragment = RepositoryDetailsFragment()
            val bundle = Bundle()
            bundle.putSerializable(Constants.REPO_BUNDLE, repository)
            fragment.arguments = bundle
            return fragment

        }
    }

}



