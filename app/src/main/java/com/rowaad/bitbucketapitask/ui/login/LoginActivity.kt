package com.rowaad.bitbucketapitask.ui.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.photoweather.app.utils.ItsText
import com.rowaad.bitbucketapitask.R
import com.rowaad.bitbucketapitask.ui.main.MainActivity
import com.rowaad.bitbucketapitask.utils.MyToast
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val viewModel: LoginViewModel by viewModel()

        setUpLoginController(viewModel)
        handleErrorMsg(viewModel)
    }

    private fun setUpLoginController(viewModel: LoginViewModel) {

        loginBtn.setOnClickListener {
            viewModel.login(userName = usernameEditText.ItsText(), passwordEditText.ItsText())
        }

        handleLoginResult(viewModel)

    }


    private fun handleLoginResult(viewModel: LoginViewModel) {
        viewModel.getLoginResults().observe(
            this, Observer { userModel ->
                startActivity(MainActivity.getStartIntent(this@LoginActivity))
                this@LoginActivity.finish()
            })
    }

    private fun handleErrorMsg(viewModel: LoginViewModel) {
        viewModel.handleErrorMsg().observe(
            this, Observer { errorMsg ->
                MyToast.showError(this@LoginActivity, errorMsg)
            })
    }
    companion object {
        fun getStartIntent(context: Context): Intent =
            Intent(context, LoginActivity::class.java)
    }

}