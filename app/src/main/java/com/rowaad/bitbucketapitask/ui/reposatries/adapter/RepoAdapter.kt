package com.rowaad.bitbucketapitask.ui.reposatries.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.photoweather.app.data.ui_models.repo.Repository
import com.rowaad.bitbucketapitask.R
import kotlinx.android.synthetic.main.item_reposatrey.view.*
import org.koin.core.KoinComponent


class RepoAdapter(
    val data: MutableList<Repository>,
    var listener: RepoListener
) : RecyclerView.Adapter<RepoAdapter.ProductsAdapterViewHolder>(), KoinComponent {


    inner class ProductsAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.setOnClickListener {
                //create any interaction listeners
                listener.onClickRepo(repository = data[adapterPosition])
            }
        }


        fun bind(item: Repository) = with(itemView) {

            repoNameTV.text = item.slug
            repoDescriptionTV.text = item.description
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsAdapterViewHolder {

        return ProductsAdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_reposatrey, null)
        )
    }

    override fun onBindViewHolder(holder: ProductsAdapterViewHolder, position: Int) =
        holder.bind(data[position])


    fun swapData(receivedData: MutableList<Repository>) {
        data.clear()
        data.addAll(receivedData)
        notifyDataSetChanged()
    }


    fun removeSingleItemData(receivedData: Repository) {
        data.remove(receivedData)
        notifyDataSetChanged()
    }


    fun removeAllData() {
        data.clear()
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int = data.size ?: 0

    interface RepoListener {
        fun onClickRepo(repository: Repository)
    }
}

