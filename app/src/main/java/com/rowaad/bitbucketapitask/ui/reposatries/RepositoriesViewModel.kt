package com.rowaad.bitbucketapitask.ui.reposatries

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.photoweather.app.data.DataManager
import com.photoweather.app.data.ui_models.repo.ReposResponse
import com.photoweather.app.inteceptor.ConnectivityStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject


class RepositoriesViewModel(
    val dataManager: DataManager
) : ViewModel(), KoinComponent {

    var disposable: Disposable? = null

    private val reposResults: MutableLiveData<ReposResponse> =
        MutableLiveData<ReposResponse>()

    private val msgError: MutableLiveData<String> =
        MutableLiveData<String>()


    val context: Context by inject()

    fun getRepos() {
        if (ConnectivityStatus.isConnected(context)) {
            when {
                else -> {
                    disposable =
                        dataManager.getRepos(
                            basicToken = dataManager.userToken ?: "",
                            userName = dataManager.user.username ?: ""
                        )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeBy(
                                onNext = {
                                    reposResults.value = it
                                    Log.e("ADfsd2", it.toString())

                                },
                                onError = {
                                    msgError.value = it.message.toString()
                                })
                }
            }
        } else {
            msgError.postValue("Please connect internet")
        }
    }

    fun getReposResults(): LiveData<ReposResponse> {
        return reposResults
    }

    fun handleErrorMsg(): LiveData<String> {
        return msgError
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

}
