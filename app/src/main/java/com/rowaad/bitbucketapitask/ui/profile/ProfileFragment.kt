package com.rowaad.bitbucketapitask.ui.profile

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.photoweather.app.data.ui_models.UserModel
import com.rowaad.bitbucketapitask.R
import com.rowaad.bitbucketapitask.ui.login.LoginActivity
import com.rowaad.bitbucketapitask.ui.reposatries.adapter.RepoAdapter
import com.rowaad.bitbucketapitask.utils.MyToast
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_repos.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : Fragment() {

    var layoutManager: GridLayoutManager? = null
    var adapter: RepoAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val viewModel: ProfileViewModel by viewModel()

        viewModel.getProfile()

        handleErrorMsg(viewModel)

        handleFetchProfile(viewModel)

        setUpController(viewModel)

    }


    private fun setUpController(viewModel: ProfileViewModel) {
        logoutBtn.setOnClickListener {
            viewModel.logout()

            startActivity(LoginActivity.getStartIntent(requireContext()))
            activity?.finish()
        }
    }


    private fun handleFetchProfile(viewModel: ProfileViewModel) {
        viewModel.getProfileResults().observe(
            viewLifecycleOwner, Observer { userModel ->
                fillProfileData(userModel)

            })
    }

    private fun fillProfileData(userModel: UserModel) {
        statusTV.text = "account status: " + userModel.account_status
        displayNameTV.text = "display name: " + userModel.display_name
        usernameTV.text = "user name: " + userModel.username
    }

    private fun handleErrorMsg(viewModel: ProfileViewModel) {
        viewModel.handleErrorMsg().observe(
            viewLifecycleOwner, Observer { errorMsg ->
                MyToast.showError(requireActivity(), errorMsg)
            })
    }


    companion object {
        fun newInstance() = ProfileFragment()
    }
}



