package com.rowaad.bitbucketapitask.di


import com.rowaad.bitbucketapitask.ui.login.LoginViewModel
import com.rowaad.bitbucketapitask.ui.profile.ProfileViewModel
import com.rowaad.bitbucketapitask.ui.reposatries.RepositoriesViewModel
import com.rowaad.bitbucketapitask.ui.splach.SplashViewModel
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel
val mainModule = module {

    viewModel { SplashViewModel(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { RepositoriesViewModel(get()) }
    viewModel { ProfileViewModel(get()) }

}