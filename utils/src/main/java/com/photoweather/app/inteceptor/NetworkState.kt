package com.photoweather.app.utils.inteceptor

enum class NetworkState {
    NO_INTERNET, NO_RESPONSE, UNAUTHORISED
}