package com.photoweather.app.data

import com.photoweather.app.data.ui_models.UserModel
import com.photoweather.app.data.ui_models.repo.ReposResponse
import io.reactivex.Observable
import retrofit2.http.Header

interface DataManager {
    var userToken: String?
    var user: UserModel

    fun isUserLogged(): Boolean

    fun logout()


    fun login(basicToken:String): Observable<UserModel>



    fun getRepos(
         basicToken: String,
        userName: String
    ): Observable<ReposResponse>

    fun getMyProfile(basicToken: String
    ): Observable<UserModel>
}