package com.photoweather.app.data.ui_models.repo

data class Mainbranch(
    val name: String,
    val type: String
)