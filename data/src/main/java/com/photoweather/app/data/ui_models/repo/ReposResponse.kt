package com.photoweather.app.data.ui_models.repo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ReposResponse(
    val page: Int,
    val pagelen: Int,
    val size: Int,
    @SerializedName("values")
    val reposatries: MutableList<Repository>
) : Serializable