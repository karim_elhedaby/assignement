package com.photoweather.app.data.utils

import android.os.RemoteException
import android.util.AndroidException
import android.util.Log
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException
import java.net.*

inline fun <reified R> String.fromJson(): R {
    return Gson().fromJson(this, R::class.java)
}

inline fun <reified R> R.toJson(): String {
    return Gson().toJson(this, R::class.java)
}

fun <T> Observable<Response<T>>.runOnMain(): Observable<Response<T>> = subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())




//check if timeout api
fun Throwable.handleException():Throwable{

    return if(this is AndroidException || this is RemoteException || this is BindException ||this is PortUnreachableException ||this is SocketTimeoutException || this is UnknownServiceException ||this is UnknownHostException || this is IOException ||this is ConnectException || this is NoRouteToHostException){
        Log.e("type",this.javaClass.toString())
        Throwable(Constants.ERROR_API.CONNECTION_ERROR)
    }
    else {
        this
    }
}

fun ResponseBody.handleError():String {
    val json = JSONObject(this.string())
    return if (json.has("errors")) {
        val errors = json.getJSONArray("errors").getJSONObject(0)
        val errorMessage = errors.getString("value")
        val key = errors.getString("key")
        errorMessage
    }
    else if (json.has("error")){
        val jsonError = json.getString("error")
        //val errorMessage = jsonError.getString("error")
        jsonError
    }
    else {
        val jsonError = json.getJSONArray("error").getJSONObject(0)
        val errorMessage = jsonError.getString("value")
        errorMessage

    }
}

