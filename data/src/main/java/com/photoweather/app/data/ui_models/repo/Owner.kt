package com.photoweather.app.data.ui_models.repo

data class Owner(
    val account_id: String,
    val display_name: String,
    val links: Links,
    val nickname: String,
    val type: String,
    val uuid: String
)