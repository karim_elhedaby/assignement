package com.photoweather.app.data.ui_models.repo

data class Project(
    val key: String,
    val links: Links,
    val name: String,
    val type: String,
    val uuid: String
)