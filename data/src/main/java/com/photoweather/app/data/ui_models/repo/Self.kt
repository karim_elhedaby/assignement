package com.photoweather.app.data.ui_models.repo

data class Self(
    val href: String
)