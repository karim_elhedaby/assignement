package com.photoweather.app.data.ui_models.repo

data class Avatar(
    val href: String
)