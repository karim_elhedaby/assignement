package com.photoweather.app.data.ui_models.repo

import java.io.Serializable

data class Repository(
    val created_on: String,
    val description: String,
    val fork_policy: String,
    val full_name: String,
    val has_issues: Boolean,
    val has_wiki: Boolean,
    val is_private: Boolean,
    val language: String,
    val name: String,
    val scm: String,
    val size: Int,
    val slug: String,
    val type: String,
    val updated_on: String,
    val uuid: String,
    val website: Any,
):Serializable