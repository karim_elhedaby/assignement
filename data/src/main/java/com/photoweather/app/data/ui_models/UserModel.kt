package com.photoweather.app.data.ui_models

import java.io.Serializable

data class UserModel(
    val account_id: String ?=null,
    val account_status: String?=null,
    val created_on: String?=null,
    val display_name: String?=null,
    val is_staff: Boolean?=null,
    val nickname: String?=null,
    val type: String?=null,
    val username: String?=null,
    val uuid: String?=null
) : Serializable
