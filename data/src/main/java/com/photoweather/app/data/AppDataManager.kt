package com.photoweather.app.data

import com.photoweather.app.data.api.retrofit.ApiHelperInterface
import com.photoweather.app.data.utils.Constants
import com.photoweather.app.data.utils.Constants.PrefKeys.TOKEN
import com.photoweather.app.data.cache.PreferencesGateway
import com.photoweather.app.data.ui_models.UserModel
import com.photoweather.app.data.ui_models.repo.ReposResponse
import com.photoweather.app.data.utils.Constants.BASE_Token
import com.photoweather.app.data.utils.Constants.PrefKeys.IS_USER_LOGGED
import com.photoweather.app.data.utils.Constants.PrefKeys.USER_DATA
import io.reactivex.Observable


class AppDataManager(
    var service: ApiHelperInterface,
    private val preference: PreferencesGateway
) : DataManager {

    override var userToken: String?
        get() = preference.load(TOKEN, BASE_Token) ?: BASE_Token
        set(value) {
            if (value == null) preference.save(TOKEN, BASE_Token)
            else preference.save(TOKEN, value)
        }


    override var user: UserModel
        get() = preference.load(
            USER_DATA,
            UserModel()
        ) ?: UserModel()
        set(value) {
            preference.save(IS_USER_LOGGED, true)
            preference.save(USER_DATA, value)
        }


    override fun isUserLogged(): Boolean {
        return preference.load(IS_USER_LOGGED, false) ?: false
    }


    override fun logout() {
        preference.clearAll()
        preference.save(IS_USER_LOGGED, false)
    }


    override fun login(basicToken: String): Observable<UserModel> {
        return service.login(basicToken)
    }

    override fun getRepos(basicToken: String, userName: String): Observable<ReposResponse> {

        return service.getRepos(basicToken, userName)
    }

    override fun getMyProfile(basicToken: String): Observable<UserModel> {
        return service.getMyProfile(basicToken)

    }

}