package com.photoweather.app.data.ui_models.repo

data class Links(
    val avatar: Avatar,
    val forks: Forks,
    val html: Html,
    val self: Self

)