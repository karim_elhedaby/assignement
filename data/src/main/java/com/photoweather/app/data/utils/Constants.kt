package com.photoweather.app.data.utils


object Constants {
    const val PACKAGE_NAME = "com.example.app"
    const val BASE_URL = "https://api.bitbucket.org/2.0/"
    const val BASE_Token = ""

    const val REPO_BUNDLE = "REPO_BUNDLE"

object PrefKeys{
    const val USER_DATA = "USER_DATA"
    const val LANG = "lang"
    const val IS_USER_LOGGED = "IS_USER_LOGGED"
    const val TOKEN = "TOKEN"
    const val PREFERENCES_NAME = "PREFERENCES_NAME"
    const val DEVICE_TOKEN = "DEVICE_TOKEN"
}



object ERROR_API{
    //deleted or unauthorized
    const val UNAUTHRIZED = "unAuthroized"
    //500
    const val SERVER_ERROR = "serverError"
    //503
    const val MAINTENANCE = "maintenanceMode"
    //400
    const val BAD_REQUEST = "bad_request"
    //
    const val CONNECTION_ERROR = "connection_error"

}


    const val INTENT_EXTRA = "extra"

    const val DEFAULT_APP_LANGUAGE = "ar"

}