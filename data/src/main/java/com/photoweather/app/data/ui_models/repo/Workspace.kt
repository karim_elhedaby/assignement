package com.photoweather.app.data.ui_models.repo

data class Workspace(
    val links: Links,
    val name: String,
    val slug: String,
    val type: String,
    val uuid: String
)