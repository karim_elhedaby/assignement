package com.photoweather.app.data.api.retrofit

import com.photoweather.app.data.ui_models.UserModel
import com.photoweather.app.data.ui_models.repo.ReposResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiHelperInterface {

    @GET("user")
    fun login(
        @Header("Authorization") basicToken: String
    ): Observable<UserModel>


    @GET("users/{userName}/repositories")
    fun getRepos(
        @Header("Authorization") basicToken: String,
        @Path("userName") userName: String
    ): Observable<ReposResponse>


    @GET("user")
    fun getMyProfile(
        @Header("Authorization") basicToken: String
    ): Observable<UserModel>

}