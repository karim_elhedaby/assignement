package com.photoweather.app.data.ui_models.repo

data class Forks(
    val href: String
)